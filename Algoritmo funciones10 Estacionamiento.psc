Funcion  N3 = estacionamiento(numero_1, numero_2, numero_3)
	si numero_2 > 0 entonces
		numero_1= numero_1+1
	FinSi
	N3 = numero_1*numero_3
FinFuncion
Algoritmo sin_titulo
	//un estacionamiento requiere determinar el cobro que debe aplicar a las personas que lo utilizan.
	//Considere que el cobro es con base en las horas que lo disponen y que las fracciones de hora se toman
	//como completas y realice un diagrama de flujo y pseudocodigo que representen el algoritmo que permita determinar el cobro
	
	Escribir "horas: "
	Leer numero_1
	Escribir "minutos: "
	Leer numero_2
	Escribir "cobro por hora: "
	Leer numero_3
	
	Escribir "Costo: " estacionamiento(numero_1, numero_2, numero_3)
	
FinAlgoritmo
